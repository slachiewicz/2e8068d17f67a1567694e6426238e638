# .bash_profile

```sh
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

export CC=clang-11
export CXX=clang++-11

export PATH=/usr/lib/linux-tools-4.15.0-66:$PATH

export PATH=$HOME/.cargo/bin:$PATH

export PATH=$PATH:/usr/local/go/bin
export GOPATH=/root/go
export PATH=$PATH:$GOPATH/bin

export PATH=$HOME/mypyc/scripts:$PATH

alias k="kubectl"
```

# Pull Docker image

https://hub.docker.com/_/ubuntu

```sh
docker pull ubuntu:20.10
```

# Install general tools

```sh
apt install -y cmake curl git iproute2 make man software-properties-common vim wget
```

- `software-properties-common` <- `add-apt-repository`


# Install utility tools

```sh
apt install dnsutils
```


# Install LLVM
 
https://apt.llvm.org/

## Ubuntu

```sh
echo "deb http://apt.llvm.org/groovy/ llvm-toolchain-groovy-11 main" | tee -a /etc/apt/sources.list
echo "deb-src http://apt.llvm.org/groovy/ llvm-toolchain-groovy-11 main" | tee -a /etc/apt/sources.list
wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
apt update
apt install -y clang-11 lldb-11 lld-11
apt install -y libclang-11-dev
```

- `libclang-11-dev` <- `BPFtrace`, `BCC`


# Install Python

```sh
apt install -y bzip2 dpkg-dev libc6-dev libbz2-dev libffi-dev libncurses5-dev libreadline-dev libsqlite3-dev libssl-dev zlib1g-dev

wget https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz
tar -xvf Python-3.9.0.tgz
cd Python-3.9.0
export LLVM_PROFDATA=/usr/bin/llvm-profdata-11  # To --enable-optimizations
./configure --enable-optimizations --without-gcc
make -j2
make altinstall
```


# Configure Git

```sh
git config --global core.editor "vim"
git config --global user.name "<name>"	
git config --global user.email "<email>"	
git config --global github.user "<username>"
git config --global credential.helper 'cache --timeout=99999999'
```


# Install Docker

https://docs.docker.com/install/linux/docker-ce/ubuntu/

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
# Equivalent?:
# echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" | tee -a /etc/apt/sources.list
# # echo "deb-src [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" | tee -a /etc/apt/sources.list
apt update
apt install docker-ce
```


# Install Docker Compose

https://docs.docker.com/compose/install/

```sh
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```


# Install Kubernetes

https://kubernetes.io/docs/tasks/tools/install-kubectl/

```sh
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
```

## Install using native package management

```sh
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
touch /etc/apt/sources.list.d/kubernetes.list
# Waiting for kubernetes-bionic dist release
echo "deb http://apt.kubernetes.io/ kubernetes-focal main" | tee -a /etc/apt/sources.list.d/kubernetes.list
apt update
apt install -y kubectl
```


# Install Google Cloud SDK

- https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu
- https://cloud.google.com/compute/docs/tutorials/python-guide

```sh
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
apt update
apt install -y google-cloud-sdk
```

# Install Node.js

https://nodejs.org/en/download/package-manager/

```sh
curl -sL https://deb.nodesource.com/setup_lts.x | bash -
apt install -y nodejs
```

# Install Java

```sh
apt install default-jre
```


# Install Rust

https://www.rust-lang.org/en-US/install.html

```sh
curl https://sh.rustup.rs -sSf | sh
```


# Install Go

https://golang.org/dl/

```sh
wget https://dl.google.com/go/go1.12.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.12.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
export GOPATH=/root/go
export PATH=$PATH:$GOPATH/bin
```


# Install Haskell

https://qiita.com/waddlaw/items/49874f4cf9b680e4b015

```sh
apt install libgmp-dev
curl -sSL https://get.haskellstack.org/ | sh
stack update
# edit stack.yaml
stack install idris
stack install agda
```


# Install OCaml

https://ocaml.org/docs/install.html
https://opam.ocaml.org/doc/Install.html

```sh
sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
apt install unzip
opam init
```


# Install BCC

https://github.com/iovisor/bcc/blob/master/INSTALL.md

```sh
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4052245BD4284CDD
echo "deb https://repo.iovisor.org/apt/bionic bionic main" | tee -a /etc/apt/sources.list.d/iovisor.list
apt update
apt install -y bcc-tools libbcc-examples
```

Or from source:

```sh
apt install bison flex g++ libelf-dev libclang-8-dev
git clone https://github.com/iovisor/bcc.git
cd bcc
mkdir build; cd build
# And edit bcc/src/python/CMakeLists.txt (remove --install-layout)
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DPYTHON_CMD=python3.7
make -j8
make install
# And cp -r /usr/lib/python3.7/site-packages/bcc /usr/local/lib/python3.7/site-packages/bcc
```


# Install BPFtrace

https://github.com/iovisor/bpftrace/blob/master/INSTALL.md

```sh
apt install bison flex g++ libelf-dev libclang-8-dev
git clone https://github.com/iovisor/bpftrace
cd bpftrace
mkdir build; cd build
cmake -DCMAKE_BUILD_TYPE=DEBUG ..
make -j8
make install
```


# Install perf

```sh
apt install -y linux-tools-common linux-tools-generic
```


# Install Nginx

https://www.nginx.com/resources/wiki/start/topics/tutorials/install/

```sh
echo "deb http://nginx.org/packages/ubuntu/ bionic nginx" | tee -a /etc/apt/sources.list
echo "deb-src http://nginx.org/packages/ubuntu/ bionic nginx" | tee -a /etc/apt/sources.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
apt update
apt install nginx
```


# Install certbot

https://certbot.eff.org/lets-encrypt/ubuntuartful-nginx

```sh
add-apt-repository ppa:certbot/certbot
apt update
apt install python-certbot-nginx python3-certbot-dns-google
```


# Install MySQL Shell

https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-install-linux-quick.html

```sh
wget https://dev.mysql.com/get/mysql-apt-config_0.8.11-1_all.deb
dpkg -i mysql-apt-config_0.8.11-1_all.deb
apt update
apt install mysql-shell
```


# Install PostgreSQL

```sh
sudo touch /etc/apt/sources.list.d/pgdg.list
echo "deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main" | tee -a /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt update
apt install postgresql-10
```


# Install PostGIS

```sh
apt install postgresql-10-postgis-2.4
```


# Install Scipy

```sh
apt install libblas-dev liblapack-dev gfortran
pip3 install scipy
```


# Install Matplotlib

```sh
apt install libfreetype6-dev
pip3 install matplotlib
```


# Install Graphviz

```sh
apt install graphviz-dev
pip3 install pygraphviz

# Or

apt install graphviz
pip3 install graphviz
```


# Install OpenCV

```sh
apt install libsm6 libxrender-dev
pip3 install opencv-python
```